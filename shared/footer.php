<?php
/* This file is part of a copyrighted work; it is distributed with NO WARRANTY.
 * See the file COPYRIGHT.html for more details.
 */

  # Be sure we don't get leftovers.
  unset($_SESSION['pageErrors']);
  unset($_SESSION['postVars']);
?>
<!-- **************************************************************************************
     * Footer
     **************************************************************************************-->
<br><br><br>
</font>
<font face="Arial, Helvetica, sans-serif" size="1" color="<?php echo H(OBIB_PRIMARY_FONT_COLOR);?>">
<center>
    <a href="http://obiblio.sourceforge.net/"><img src="../images/powered_by_openbiblio.gif" width="125" height="44" border="0"></a>
  <br><br>
  <?php echo $headerLoc->getText("footerPoweredBy"); ?> <?php echo H(OBIB_CODE_VERSION);?>,
  <?php echo $headerLoc->getText("footerDatabaseVersion"); ?> <?php echo H(OBIB_DB_VERSION);?>,
  <?php echo $headerLoc->getText("footerCopyright"); ?> &copy; 2002-2014 Dave Stevens, et al.<br>
  CrowdBiblio patches &copy; 2015 Wolfram Sang<br>
  <?php echo $headerLoc->getText("footerUnderThe"); ?>
  <a href="../shared/copying.html"><?php echo $headerLoc->getText("footerGPL"); ?></a>
</center>
<br>
</font>
    </td>
  </tr>
</table>
</body>
</html>
