<?php
/* This file is part of a copyrighted work; it is distributed with NO WARRANTY.
 * See the file COPYRIGHT.html for more details.
 */
 
  require_once("../shared/common.php");
  $tab = "circulation";
  $restrictToMbrAuth = TRUE;
  $nav = "deletedone";
  $restrictInDemo = true;
  require_once("../shared/logincheck.php");
  require_once("../classes/MemberQuery.php");
  require_once("../classes/StaffQuery.php");
  require_once("../classes/BiblioStatusHistQuery.php");
  require_once("../classes/MemberAccountQuery.php");
  require_once("../classes/BiblioCopyQuery.php");
  require_once("../classes/BiblioSearchQuery.php");
  require_once("../functions/errorFuncs.php");
  require_once("../classes/Localize.php");
  $loc = new Localize(OBIB_LOCALE,$tab);

  $mbrid = $_GET["mbrid"];
  $mbrName = $_GET["name"];

  #**************************************************************************
  #*  Get list of all owner's copies
  #**************************************************************************
  $biblioQ = new BiblioSearchQuery();
  $biblioQ->connect();
  if ($biblioQ->errorOccurred()) {
    $biblioQ->close();
    displayErrorPage($biblioQ);
  }
  if (!$biblioQ->doQuery('','',$mbrid)) {
    $biblioQ->close();
    displayErrorPage($biblioQ);
  }

  $copyQ = new BiblioCopyQuery();
  $copyQ->connect();
  if ($copyQ->errorOccurred()) {
    $copyQ->close();
    displayErrorPage($copyQ);
  }

  $histQ = new BiblioStatusHistQuery();
  $histQ->connect();
  if ($histQ->errorOccurred()) {
    $histQ->close();
    displayErrorPage($histQ);
  }

  while ($biblio = $biblioQ->fetchRow()) {
    #**************************************************************************
    #*  Delete Bibliography Copy
    #**************************************************************************
    #FIXME: check if copy is checked out?
    if (!$copyQ->delete($biblio->getBibid(),$biblio->getCopyid())) {
      $copyQ->close();
      displayErrorPage($copyQ);
    }
    $copyQ->close();

    #**************************************************************************
    #*  Delete Copy History
    #**************************************************************************
    if (!$histQ->deleteByBibid($biblio->getBibid(),$biblio->getCopyid())) {
      $histQ->close();
      displayErrorPage($histQ);
    }
    $histQ->close();
  }

  #**************************************************************************
  #*  Delete library member
  #**************************************************************************
  $mbrQ = new MemberQuery();
  $mbrQ->connect();
  $mbr = $mbrQ->get($mbrid);
  $mbrQ->delete($mbrid);
  $mbrQ->close();

  #**************************************************************************
  #*  Delete Member History
  #**************************************************************************
  $histQ = new BiblioStatusHistQuery();
  $histQ->connect();
  if ($histQ->errorOccurred()) {
    $histQ->close();
    displayErrorPage($histQ);
  }
  if (!$histQ->deleteByMbrid($mbrid)) {
    $histQ->close();
    displayErrorPage($histQ);
  }
  $histQ->close();

  #**************************************************************************
  #*  Delete Member Account
  #**************************************************************************
  $transQ = new MemberAccountQuery();
  $transQ->connect();
  if ($transQ->errorOccurred()) {
    $transQ->close();
    displayErrorPage($transQ);
  }
  $trans = $transQ->delete($mbrid);
  if ($transQ->errorOccurred()) {
    $transQ->close();
    displayErrorPage($transQ);
  }
  $transQ->close();

  #**************************************************************************
  #*  Delete staff member
  #**************************************************************************
  $staffQ = new StaffQuery();
  $staffQ->connect();
  if ($staffQ->errorOccurred()) {
    $staffQ->close();
    displayErrorPage($staffQ);
  }
  if (!$staffQ->deleteName($mbr->getBarcodeNmbr())) {
    $staffQ->close();
    displayErrorPage($staffQ);
  }
  $staffQ->close();

  #**************************************************************************
  #*  Show success page
  #**************************************************************************
  require_once("../shared/header.php");
  echo $loc->getText("mbrDelSuccess",array("name"=>$mbrName));
  
?>
<br><br>
<a href="../circ/index.php"><?php echo $loc->getText("mbrDelReturn");?></a>
<?php require_once("../shared/footer.php"); ?>
