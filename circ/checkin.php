<?php
/* This file is part of a copyrighted work; it is distributed with NO WARRANTY.
 * See the file COPYRIGHT.html for more details.
 */
 
  require_once("../shared/common.php");
  $tab = "circulation";
  $nav = "checkin";
  $restrictInDemo = true;
  require_once("../shared/logincheck.php");

  require_once("../classes/BiblioCopy.php");
  require_once("../classes/BiblioCopyQuery.php");
  require_once("../functions/errorFuncs.php");
  require_once("../classes/Localize.php");
  $loc = new Localize(OBIB_LOCALE,$tab);

  #****************************************************************************
  #*  Checking for post vars.  Go back to form if none found.
  #****************************************************************************
  $bibids = "";
  $copyids = "";

  if (count($_GET) != 0) {
    $massCheckin = FALSE;
    $bibids[] = $_GET["bibid"];
    $copyids[] = $_GET["copyid"];
  } else {
  # Broke the indentation to keep the diff readable :/
  if (count($_POST) == 0) {
    header("Location: ../circ/checkin_form.php?reset=Y");
    exit();
  }
  $massCheckinFlg = $_POST["massCheckin"];
  if ($massCheckinFlg == "Y") {
    $massCheckin = TRUE;
  } else {
    $massCheckin = FALSE;
  }
  if (!$massCheckin) {
    foreach($_POST as $key => $value) {
      if ($value == "copyid") {
        parse_str($key,$output);
        $bibids[] = $output["bibid"];
        $copyids[] = $output["copyid"];
      }
    }
  }
  if ((!$massCheckin) and (!is_array($bibids))) {
    $msg = $loc->getText("checkinErr1");
    header("Location: ../circ/checkin_form.php?reset=Y&msg=".U($msg));
    exit();
  }
  }
  #**************************************************************************
  #*  Checkin bibliographies in bibidList
  #**************************************************************************
  $copyQ = new BiblioCopyQuery();
  $copyQ->connect();
  if ($copyQ->errorOccurred()) {
    $copyQ->close();
    displayErrorPage($copyQ);
  }

  # Only owner of a book is allowed to checkin. Currently limited to _GET case.
  if (count($_GET) != 0) {
    if (!$copy = $copyQ->doQuery($bibids[0],$copyids[0])) {
      $copyQ->close();
      displayErrorPage($copyQ);
    }

    if (!$copy->isOwnerOnline()) {
      $msg = $loc->getText("biblioCopyOwnerErr1");
      $copyQ->close();
      header("Location: ../circ/mbr_view.php?mbrid=".U($copy->getMbrid())."&msg=".U($msg));
      exit();
    }
  }

  if (!$copyQ->checkin($massCheckin,$bibids,$copyids)) {
    $copyQ->close();
    displayErrorPage($copyQ);
  }
  $copyQ->close();

  #**************************************************************************
  #*  Destroy form values and errors
  #**************************************************************************
  unset($_SESSION["postVars"]);
  unset($_SESSION["pageErrors"]);

  #**************************************************************************
  #*  Go back to member view
  #**************************************************************************
  if (count($_GET) != 0) {
	require_once("../classes/BiblioStatusHist.php");
	require_once("../classes/BiblioStatusHistQuery.php");

	$hist = new BiblioStatusHist();
	$hist->setBibid($copy->getBibid());
	$hist->setCopyid($copy->getCopyid());
	$hist->setStatusCd($copy->getStatusCd());
	$hist->setStatusBeginDt($copy->getStatusBeginDt());
	$hist->setMbrid($copy->getMbrid());

	$histQ = new BiblioStatusHistQuery();
	$histQ->connect();
	if ($histQ->errorOccurred())
		displayErrorPage($histQ);

	//if (!$histQ->insert($hist)) {
	//	$histQ->close();
	//	displayErrorPage($histQ);
	//}
	$histQ->close();

    header("Location: ../circ/mbr_view.php?mbrid=".U($copy->getMbrid()));
  } else {
    header("Location: ../circ/checkin_form.php?reset=Y");
  }
?>
