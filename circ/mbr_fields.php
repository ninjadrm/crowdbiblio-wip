<?php
/* This file is part of a copyrighted work; it is distributed with NO WARRANTY.
 * See the file COPYRIGHT.html for more details.
 */

  require_once("../functions/inputFuncs.php");
  require_once('../classes/DmQuery.php');
  $dmQ = new DmQuery();
  $dmQ->connect();
  $mbrClassifyDm = $dmQ->getAssoc('mbr_classify_dm');
  $customFields = $dmQ->getAssoc('member_fields_dm');
  $dmQ->close();
  $show = $_SESSION["hasCircMbrAuth"];

  $fields = array(
    "mbrFldsClassify" => inputField($show ? 'select' : 'hidden', 'classification', $mbr->getClassification(), NULL, $mbrClassifyDm) . ($show ? '' : $mbrClassifyDm[$mbr->getClassification()]),
    "mbrFldsCardNmbr" => inputField($show ? 'text' : 'hidden', "barcodeNmbr", $mbr->getBarcodeNmbr()) . ($show ? '' : $mbr->getBarcodeNmbr()),
    "mbrFldsLastName" => inputField($show ? 'text' : 'hidden', "lastName", $mbr->getLastName()) . ($show ? '' : $mbr->getLastName()),
    "mbrFldsFirstName" => inputField($show ? 'text' : 'hidden', "firstName", $mbr->getFirstName()) . ($show ? '' : $mbr->getFirstName()),
    "mbrFldsEmail" => inputField('text', "email", $mbr->getEmail()),
    "Mailing Address:" => inputField('textarea', "address", $mbr->getAddress()),
    "mbrFldsHomePhone" => inputField('text', "homePhone", $mbr->getHomePhone()),
    "mbrFldsWorkPhone" => inputField('text', "workPhone", $mbr->getWorkPhone()),
    "mbrFldsMbrShip" => inputField($show ? 'text' : 'hidden', "membershipEnd", $mbr->getMembershipEnd())
			. ( $show ? '' : ($mbr->getMembershipEnd()=="0000-00-00" ? $loc->getText("mbrViewMbrShipNoEnd") : $mbr->getMembershipEnd())),
  );
  
  foreach ($customFields as $name => $title) {
    $fields[$title.':'] = inputField('text', 'custom_'.$name, $mbr->getCustom($name));
  }
?>

<table class="primary">
  <tr>
    <th colspan="2" valign="top" nowrap="yes" align="left">
      <?php echo H($headerWording);?> <?php echo $loc->getText("mbrFldsHeader"); ?>
    </td>
  </tr>
<?php
  foreach ($fields as $title => $html) {
?>
  <tr>
    <td nowrap="true" class="primary" valign="top">
      <?php echo $loc->getText($title); ?>
    </td>
    <td valign="top" class="primary">
      <?php echo $html; ?>
    </td>
  </tr>
<?php
  }
?>
  <tr>
    <td align="center" colspan="2" class="primary">
      <input type="submit" value="<?php echo $loc->getText("mbrFldsSubmit"); ?>" class="button">
      <input type="button" onClick="self.location='<?php echo H(addslashes($cancelLocation));?>'" value="<?php echo $loc->getText("mbrFldsCancel"); ?>" class="button">
    </td>
  </tr>

</table>
