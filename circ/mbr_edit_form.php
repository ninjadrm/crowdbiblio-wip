<?php
/* This file is part of a copyrighted work; it is distributed with NO WARRANTY.
 * See the file COPYRIGHT.html for more details.
 */
 
  require_once("../shared/common.php");
  session_cache_limiter(null);

  $tab = "circulation";
  if (isset($_GET["mbrid"])){
    $mbrid = $_GET["mbrid"];
  } else {
    require("../shared/get_form_vars.php");
    $mbrid = $postVars["mbrid"];
  }
  if ($mbrid != $_SESSION["mbrid"]) {
    $restrictToMbrAuth = TRUE;
  }
  $nav = "mbr_edit";
  $focus_form_name = "editMbrform";
  $focus_form_field = "barcodeNmbr";
  require_once("../functions/inputFuncs.php");
  require_once("../shared/logincheck.php");

  require_once("../classes/Member.php");
  require_once("../classes/MemberQuery.php");

  $mbrQ = new MemberQuery();
  $mbrQ->connect();
  $mbr = $mbrQ->get($mbrid);
  $mbrQ->close();
  require_once("../shared/header.php");
  require_once("../classes/Localize.php");
  $loc = new Localize(OBIB_LOCALE,$tab);
  $headerWording = $loc->getText("mbrEditForm");
  $adm_loc = new Localize(OBIB_LOCALE,'admin');

  $cancelLocation = "../circ/mbr_view.php?mbrid=".$mbrid."&reset=Y";
?>

<form name="editMbrform" method="POST" action="../circ/mbr_edit.php">
<input type="hidden" name="mbrid" value="<?php echo H($mbrid);?>">
<?php include("../circ/mbr_fields.php"); ?>
</form>

<br>

<form name="pwdresetform" method="POST" action="../circ/pwd_reset.php">
<table class="primary">
  <tr>
    <th colspan="2" valign="top" nowrap="yes" align="left">
      <?php echo $adm_loc->getText("adminStaff_pwd_reset_form_Resetheader"); ?>
    </th>
  </tr>
  <tr>
    <td nowrap="true" class="primary">
      <?php echo $adm_loc->getText("adminStaff_new_form_Password"); ?>
    </td>
    <td valign="top" class="primary">
      <input type="password" name="pwd" size="20" maxlength="20"
      value="<?php if (isset($postVars["pwd"])) echo H($postVars["pwd"]); ?>" ><br>
      <font class="error">
      <?php if (isset($pageErrors["pwd"])) echo H($pageErrors["pwd"]); ?></font>
    </td>
  </tr>
  <tr>
    <td nowrap="true" class="primary">
      <?php echo $adm_loc->getText("adminStaff_new_form_Reenterpassword"); ?>
    </td>
    <td valign="top" class="primary">
      <input type="password" name="pwd2" size="20" maxlength="20"
      value="<?php if (isset($postVars["pwd2"])) echo H($postVars["pwd2"]); ?>" ><br>
      <font class="error">
      <?php if (isset($pageErrors["pwd2"])) echo H($pageErrors["pwd2"]); ?></font>
    </td>
  </tr>
  <tr>
    <td align="center" colspan="2" class="primary">
      <input type="submit" value="<?php echo $adm_loc->getText("adminSubmit"); ?>" class="button">
      <input type="button" onClick="self.location='<?php echo $cancelLocation;?>'" value="  <?php echo $adm_loc->getText("adminCancel"); ?>  " class="button">
    </td>
  </tr>
</table>
</form>

<?php include("../shared/footer.php"); ?>
