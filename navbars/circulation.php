<?php
/* This file is part of a copyrighted work; it is distributed with NO WARRANTY.
 * See the file COPYRIGHT.html for more details.
 */
 
  require_once("../classes/Localize.php");
  $navloc = new Localize(OBIB_LOCALE,"navbars");
 
?>
<input type="button" onClick="self.location='../shared/logout.php'" value="<?php echo $navloc->getText("Logout"); ?>" class="navbutton"><br />
<br />

<a href="../circ/mbr_view.php?mbrid=<?php echo $_SESSION["mbrid"]; ?>&amp;reset=Y" class="alt1"><?php echo $navloc->getText("myData"); ?></a><br>
<a href="../circ/mbr_view_copies.php?mbrid=<?php echo $_SESSION["mbrid"]; ?>&amp;reset=Y" class="alt1"><?php echo $navloc->getText("myCopies"); ?></a><br>
<?php if ($nav == "searchform") { ?>
 &raquo; <?php echo $navloc->getText("memberSearch"); ?><br>
<?php } else { ?>
 <a href="../circ/index.php" class="alt1"><?php echo $navloc->getText("memberSearch"); ?></a><br>
<?php } ?>

<?php if ($nav == "search") { ?>
 &nbsp; &raquo; <?php echo $navloc->getText("catalogResults"); ?><br>
<?php } ?>

<?php
if (substr($nav, 0, 4) == "mbr_") {
  if ($nav == "mbr_view")
    echo '&raquo; '. $navloc->getText("memberInfo") . '<br>';
  else
    echo '&nbsp;&nbsp; <a href="../circ/mbr_view.php?mbrid=' . HURL($mbrid) . '" class="alt1">' . $navloc->getText("memberInfo") . '</a><br>';

  if ($nav == "mbr_view_copies")
    echo '&raquo; '. $navloc->getText("memberCopies") . '<br>';
  else
    echo '&nbsp;&nbsp; <a href="../circ/mbr_view_copies.php?mbrid=' . HURL($mbrid) . '" class="alt1">' . $navloc->getText("memberCopies") . '</a><br>';

  if ($mbrid == $_SESSION["mbrid"] || $_SESSION["hasCircMbrAuth"]) {
    if ($nav == "mbr_edit")
      echo '&raquo; '. $navloc->getText("editInfo") . '<br>';
    else
      echo '&nbsp;&nbsp; <a href="../circ/mbr_edit_form.php?mbrid=' . HURL($mbrid) . '" class="alt1">' . $navloc->getText("editInfo") . '</a><br>';

    if ($nav == "mbr_hist")
      echo '&raquo; '. $navloc->getText("checkoutHistory") . '<br>';
    else
      echo '&nbsp;&nbsp; <a href="../circ/mbr_history.php?mbrid=' . HURL($mbrid) . '" class="alt1">' . $navloc->getText("checkoutHistory") . '</a><br>';

   if (0) {
    if ($nav == "mbr_account")
      echo '&raquo; '. $navloc->getText("account") . '<br>';
    else
      echo '&nbsp;&nbsp; <a href="../circ/mbr_account.php?mbrid=' . HURL($mbrid) . '&amp;reset=Y" class="alt1">' . $navloc->getText("account") . '</a><br>';
   }
  }

  if ($_SESSION["hasCircMbrAuth"]) {
    if ($nav == "mbr_delete")
      echo '&raquo; '. $navloc->getText("catalogDelete") . '<br>';
    else
      echo '&nbsp;&nbsp; <a href="../circ/mbr_del_confirm.php?mbrid=' . HURL($mbrid) . '" class="alt1">' . $navloc->getText("catalogDelete") . '</a><br>';
  }
}
?>


<?php
if ($_SESSION["hasCircMbrAuth"]) {
  if ($nav == "new") { ?>
 &raquo; <?php echo $navloc->getText("newMember"); ?><br>
<?php } else { ?>
 <a href="../circ/mbr_new_form.php?reset=Y" class="alt1"><?php echo $navloc->getText("newMember"); ?></a><br>
<?php }
} ?>

<?php if ($_SESSION["hasAdminAuth"]) { ?>
<?php if ($nav == "checkin") { ?>
 &raquo; <?php echo $navloc->getText("checkIn"); ?><br>
<?php } else { ?>
 <a href="../circ/checkin_form.php?reset=Y" class="alt1"><?php echo $navloc->getText("checkIn"); ?></a><br>
<?php } ?>

<?php if ($nav == "offline") { ?>
 &raquo; <?php echo $navloc->getText("Offline Circulation"); ?><br>
<?php } else { ?>
 <a href="../circ/offline.php" class="alt1"><?php echo $navloc->getText("Offline Circulation"); ?></a><br>
<?php }
} ?>
